# README #

## Instructions ##

### Breakout ###
Extend a Breakout-style game implemented in Unity. If you are unfamiliar with the game, there are many versions you can play for free online to get an idea of what we’re looking for.

We also value creativity from every member of our teams, so we’d like to see you try to put your spin on these classic mechanics. Finally, if any of these instructions aren’t clear or you have trouble getting the project set up, please feel free to reach out to us for help!

### Existing Functionality ###
This functionality is already implemented in the existing game project:

* The ball:
	* The ball should rebound off the sides, top of the game board and off the paddle.
* The Paddle:
	* Moves left and right with input.
* The Player:
	* If the ball falls off the bottom of the board the game is over or a life is lost and the ball is reset.
	* After losing all lives the game ends.
* The Game:
	* Has a main menu.
	* A single level.
	* Restarts correctly during subsequent playthroughs without needing to close and reopen the game.
	* The game should be playable from the Unity Editor.

### New Functional Requirements ###
We would like you to extend this game to include the following new mechanics:

Aim to spend around 3 - 4 hours.

1. Bricks, new objects that the ball can hit and bounce off:
	1. Standard Brick:
		* Once hit the brick is destroyed, and the ball is rebounded.
	1. Multi Hit Brick:
		* The brick needs to be hit multiple times before being destroyed.
	1. The Moving Brick:
		* A brick that moves left and right across the level. 1 Hit to destroy.
	1. The Brick Brick:
		* An indestructible brick that cannot be destroyed.
1. A Level that has at least one of each type of brick. 
1. Paddle Boost:
	* If the player clicks when the ball is near the paddle, the ball is accelerated away from the paddle at a higher velocity.
1. The existing functionality should be maintained.
	1. Restarts correctly during subsequent playthroughs without needing to close and reopen the game.
	1. The game should be playable from the Unity Editor.


### What we're looking for ###
* The process you used to complete this test.
* Good use of Object Orientated design.
* Code reusability.
* Extensibility and maintainability of the software.
* Use of appropriate Unity capabilities.
* Use of best practises.
* Your creativity in making the game fun.

### Deliverables ###
* The source for your project hosted on [bitbucket.org](https://bitbucket.org).
* Instructions on how to play your game.


## Technology ##
* Unity 2019.4.
* You can use assets (excluding code) from the Unity Asset Store, however please note what assets you have used and why you have used them.

## Next Steps ##
1. Install Unity 2019.4.
1. [Fork](../../fork) this repository to your bitbucket account as a private repository, then clone it to your local machine.
1. Open the Unity project and check to make sure the existing functionality is working correctly.
1. Implement the required new functionality.
1. Submit to your repository as needed.
1. Once you are satisfied with your submission, ensure the developers included in the email you received about this test have access to the repository, and then reply to that email to let us know! We’ll get in touch once we’ve had a chance to review it.

## Extra Thoughts ##
Once you have finished your submission, think about this question:

* How would you implement multiple levels in this project?