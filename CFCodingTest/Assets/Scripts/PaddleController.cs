﻿using UnityEngine;

public class PaddleController : MonoBehaviour
{
    [SerializeField] private Camera _camera = null;
    [SerializeField] private float _paddleMinX = -10f;
    [SerializeField] private float _paddleMaxX = 10f;

    [SerializeField] private Ball _ball;
    [SerializeField] private float _boostDistanceMin;
    [SerializeField] private float _boostDistanceMax;

    private Vector3 _position;
    
    // Awake is called before the first frame update
    private void Awake()
    {
        _position = transform.position;
        
        if (_camera == null)
        {
            _camera = Camera.main;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        // Update paddle position
        Vector2 mouseWorldPos = _camera.ScreenToWorldPoint(Input.mousePosition);
        _position.x = Mathf.Clamp(mouseWorldPos.x, _paddleMinX, _paddleMaxX);
        transform.position = _position;

        // on mouse click
        if (Input.GetMouseButtonDown(0))
        {
            if (_ball == null || _ball.state != Ball.State.InMotion)
                return;

            var dist = (_ball.transform.position - transform.position).magnitude;
            if (dist >= _boostDistanceMin && dist <= _boostDistanceMax)
            {
                _ball.Boost();
            }
        }
    }

    // OnApplicationFocus is called when the game window gets or loses focus 
    private void OnApplicationFocus(bool hasFocus)
    {
        // Hide cursor during gameplay
        Cursor.visible = !hasFocus;
    }
}
