﻿using System;
using UnityEngine;

public enum BrickType { None = -1, Standard, MultiHit, Moving, Indestructible }

public class Brick : MonoBehaviour
{
    [SerializeField] private int _maxHealth;
    [SerializeField] private BrickType _brickType;

    private int _currentHealth;

    private void Start()
    {
        _currentHealth = _maxHealth;
    }

    public virtual void ReduceHeatlh(int damage, Action<Brick> callback)
    {
        if (damage <= 0 || _brickType == BrickType.Indestructible)
            return;

        _currentHealth -= damage;
        if(_currentHealth <= 0)
        {
            _currentHealth = 0;
            callback?.Invoke(this);
        }        
    }

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }

    public void Revive()
    {
        _currentHealth = _maxHealth;
        SetActive(true);
    }
}
