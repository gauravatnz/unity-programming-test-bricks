﻿using UnityEngine;

public class MovingBrick : Brick
{
    [Range(1,4)] [SerializeField] private float _brickX = 1;
    [SerializeField] private float _speed = 1;
    private float ElapsedTime = 0f;

    private void FixedUpdate()
    {
        ElapsedTime += Time.fixedDeltaTime;
        float x = _brickX * Mathf.Sin(ElapsedTime * _speed);
        transform.position = new Vector3(x, transform.position.y);
    }
}
