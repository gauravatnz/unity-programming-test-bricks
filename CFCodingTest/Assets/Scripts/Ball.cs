﻿using System;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public enum State
    {
        Ready,
        InMotion,
        Stopped
    }

    [SerializeField] private float _speed = 2f;
    [SerializeField] private float _bounceAngleAtPaddleEdges = 45f;
    [SerializeField] private CircleCollider2D _collider = null;
    [SerializeField] private int _damage = 1;
    [SerializeField] private float _boost = 1.8f;
    
    public State state { get; private set; } = State.Ready;
    public Vector2 momentum { get; private set; } = Vector2.zero;

    public event Action BallLostEvent = delegate { };
    public event Action<Brick> HitBrickEvent = delegate { };

    // Reset the ball to its initial state
    public void Initialize(Transform paddleAnchor)
    {
        transform.SetParent(paddleAnchor);
        transform.localPosition = Vector3.zero;
        momentum = Vector2.zero;
        state = State.Ready;
    }
    
    // Launch the ball from the paddle 
    public void Launch()
    {
        if (state == State.Ready)
        {
            transform.SetParent(null);
            momentum = new Vector2(0f, 1f);
            state = State.InMotion;
        }
    }

    public void SetState(State state)
    {
        this.state = state;
    }

    public void Boost()
    {
        momentum = new Vector2(momentum.x, momentum.y * -1 * _boost);
    }

    // FixedUpdate is called once per physics tick
    private void FixedUpdate()
    {
        if (state != State.InMotion)
        {
            return;
        }

        Vector2 newPosition = transform.position;
        float remainingMoveDistance = _speed * Time.fixedDeltaTime;
        int test = 0;
        while (remainingMoveDistance > 0 && test < 10)
        {
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, _collider.radius, momentum, remainingMoveDistance);
            if (hit.collider != null)
            {
                if (hit.collider.CompareTag("Hazard"))
                {
                    // Ball lost!
                    state = State.Stopped;
                    momentum = Vector2.zero;
                    
                    Vector2 positionAtCollision = hit.point + _collider.radius * hit.normal;
                    newPosition = positionAtCollision;
                    
                    BallLostEvent();
                }
                else if (hit.collider.CompareTag("Player"))
                {
                    // Bounced off paddle
                    float offsetFromPaddle = hit.transform.position.x - transform.position.x;
                    float angularStrength = offsetFromPaddle / hit.collider.bounds.size.x;
                    float bounceAngle = -_bounceAngleAtPaddleEdges * angularStrength * Mathf.Deg2Rad;
                    momentum = new Vector2(Mathf.Sin(bounceAngle), Mathf.Cos(bounceAngle)).normalized;
                    
                    Vector2 positionAtBounce = hit.point + _collider.radius * hit.normal;
                    remainingMoveDistance -= Vector2.Distance(newPosition, positionAtBounce);
                    newPosition = positionAtBounce;

                    test++;
                }
                else
                {
                    // Bounced off bounds, etc.
                    momentum = Vector2.Reflect(momentum, hit.normal);
                
                    Vector2 positionAtBounce = hit.point + _collider.radius * hit.normal;
                    remainingMoveDistance -= Vector2.Distance(newPosition, positionAtBounce);
                    newPosition = positionAtBounce;

                    // hit with bricks
                    if (hit.collider.CompareTag("Brick"))
                    {
                        var brick = hit.collider.GetComponent<Brick>();
                        if (brick != null)
                        {
                            brick.ReduceHeatlh(_damage, HitBrickEvent);
                        }
                    }
                }
            }
            else
            {
                newPosition += momentum * remainingMoveDistance;
                break;
            }
        }

        transform.position = newPosition;
    }
}
