﻿using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private Ball _ball = null;
    [SerializeField] private Transform _paddleAnchor = null;
    
    [SerializeField] private float _resetBallDelay = 2f;
    [SerializeField] private int _startingLives = 3;
    
    [SerializeField] private GameObject[] _livesCounterIcons = new GameObject[0];
    [SerializeField] private GameObject _gameOverScreen = null;

    [SerializeField] private Transform _level;

    private int _totalBricks;

    private int _lives;
    public int lives
    {
        get => _lives;
        private set
        {
            _lives = value;
            
            // Update lives counter
            for (int i = 0; i < _livesCounterIcons.Length; i++)
            {
                _livesCounterIcons[i].SetActive(i < lives);
            }
        }
    }

    private void Awake()
    {
        _ball.BallLostEvent += OnBallLost;
        _ball.HitBrickEvent += OnHitBrick;

        _totalBricks = _level.childCount;

        RestartGame();
    }

    private void OnDestroy()
    {
        _ball.BallLostEvent -= OnBallLost;
        _ball.HitBrickEvent -= OnHitBrick;
    }

    public void RestartGame()
    {
        _gameOverScreen.SetActive(false);
        Cursor.visible = false;
        
        _ball.Initialize(_paddleAnchor);
        lives = _startingLives;

        for (int i = 0; i < _level.childCount; i++)
        {
            var brick = _level.GetChild(i).GetComponent<Brick>();
            if (brick != null)
                brick.Revive();
        }
            
        _totalBricks = _level.childCount;
    }

    private void Update()
    {
        // Launch ball
        if (_ball != null && _ball.state == Ball.State.Ready)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _ball.Launch();
            }
        }
    }
    
    private void OnBallLost()
    {
        if (lives > 0)
        {
            StartCoroutine(ResetBallProcess());
        }
        else
        {
            StartCoroutine(GameOverProcess());
        }
    }

    private void OnHitBrick(Brick brick)
    {
        if (_level == null)
            return;

        brick.SetActive(false);
        _totalBricks -= 1;

        if (_totalBricks <= 0)
        {
            _ball.SetState(Ball.State.Stopped);
            StartCoroutine(GameOverProcess()); 
        }
    }

    private IEnumerator ResetBallProcess()
    {
        yield return new WaitForSeconds(_resetBallDelay);
        
        _ball.Initialize(_paddleAnchor);
        lives--;
    }

    private IEnumerator GameOverProcess()
    {
        yield return new WaitForSeconds(_resetBallDelay);
        
        _gameOverScreen.SetActive(true);
        Cursor.visible = true;
    }
}
